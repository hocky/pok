# Pembahasan Pretest POK

## Nomor 1
Lengkapi definisi jenis operasi dalam daur eksekusi instruksi berikut ini dengan cara menjodohkannya dengan definisi yang sesuai

- Instruction Decode - Menerjemahkan Kode Instruksi
- Execute - Eksekusi Instruksi
- Instruction Fetch - Mengambil kode instruksi dari memori ke IR (Instruction Register)

## Nomor 2

Diketahui
- Cache memory size = $1$ KB = $2^{10}$ Bytes
- Block size = $16$ Bytes = $2^{4}$ Bytes
- Main Memory Size = $1$ MB = $2^{20}$ Bytes

Dicari
- Number of Bits in Physical Address, from main memory size = $20$ bits
- offset is from block size = $4$ bits
- line is from Cache/Block = $6$ Bits
- tag is from $20-(4+6)$ = $10$ Bits
- block number is line+tag = $14$ Bits
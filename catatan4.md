### Arrays

Variabel **h** diasosiasikan dengan register $s2, Array **A** punya alamat dasar di $s3.

```c
A[3] = h + A[1];
```

Transfer A[1] dari memori ke register
```mips
lw $t0, 4($s3) # offset = 4
```

Lakukan penjumlahan
```mips
add $t0, $s2, $t0
```

Ingin disimpan di A[3]
```
sw $t0, 12($s3) # off set 3 × 4 = 12
```

### Address versus Value

Harus dapat dibedakan yang kita operasikan ini alamat, atau data. Saat mau mengakses array, kita harus memanipulasi address, yang disimpan di dalam register ialah address, bukan data.

Namun saat data disimpan di dalam register ingin dioperasikan, maka yang dioperasikan ialah data. Bukan alamat. Jadi kita harus mengetahui apa yang sedang kita operasikan. Address atau value. Tidak akan ada report bahwa ini error. Karena bisa saja address dan data dioperasikan, namun kita harus tahu bahwa ini salah.

misal `add $t2, $t1, $t0` maka kita tahu bahwa $t0 dan $t1 ialah data. Jika kita menulisnya `lw $t2, 0($t0)`, maka $t0 menyimpan alamat memori.
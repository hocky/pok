## Performance

### Performance Definition

Kinerja atau Performance dalam kehidupan sehari-hari ada beberapa perspektif atau pandangan, yakni sebagai berikut.

- Purchasing Perspective, kita adalah orang yang akan membeli, user yang akan menggunakan/
- Design Perspective, kita adalah perancang, yang mendesain.

Dari kedua pandangan ini, mempunyai angle dan pengukuran yang berbeda.

Saat kita membeli sesuatu, kita pasti akan mencari harga yang murah, namun performancenya tinggi, canggih. Sebagai perancang, tujuan kita ingin membuat sesuatu yang lebih baik dari yang sebelumnya.

Jawabannya berbeda dari kedua pandangan tersebut. Apakah dihitung dari yang paling murah? Atau rasionya? Ada banyak kriterianya dan masing-masing valid.

Selain itu, harus ada bagaimana, atau dasar cara mengukurnya, serta nilai untuk evaluasinya, apa dasar perbandingannya.

Pada topik ini, kita akan mengukur kinerja suatu mesin dari sudut pandang perancang.

Misalkan pada pesawat, kita bisa membuat parameter kapasitas penumpang, jarak tempuh, kecepatan, serta jumlah penumpang dikali kecepatan (berapa penumpang dan seberapa jauh yang bisa diangkut dalam satuan waktu, efektivitas, **throughput**).

Kalau misalkan kita ingin beli pesawat, tentunya kita akan menentukan berdasarkan kriterianya. Misalnya dari kecepatan, jarak tempuh, atau efektivitas pengangkutan, atau jumlah penumpang.

Pada komputer kita bisa melihat kriteria berdasarkan waktu reaksi terhadap tugas yang diberikan, pengguna yang bisa menggunakan, ataupun berapa tugas yang dapat diselesaikan dalam suatu waktu.

Selanjutnya, kita harus mengetahui bagaimana cara mengukur kriteria tersebut, setelah mengetahui apa yang ingin diukur.

#### Response time/Execution time/latency

Ialah waktu yang dibutuhkan dari suatu pekerjaan dimulai sampai selesai mengukurnya ialah dapat ditanya.

- Berapa lama untuk melaksanakan satu tugas?
- Berapa lama harus menunggu query database?

#### Throughput

Total pekerjaan yang dilakukan dalam suatu waktu.

- Berapa banyak tugas/pekerjaan yang dapat dilakukan bersamaan?
- Seberapa cepat rata-rata waktu eksekusinya?

<hr>

Saat kita mengupgrade suatu mesin dengan komponen baru, harus tahu apa yang harus ditingkatkan.

- Kalau update prosesor, meningkatkan response time dan throughput.
- Kalau menambahkan mesin, meningkatkan throughput.

Performance atau kinerja dihitung dalam satuan berapa unit yang selesai per satuan waktu, semakin besar angkanya, semakin besar. ✔️

Response time atau waktu respon, semakin kecil waktunya makan semakin baik. ✔️

Performance dari suatu mesin berbanding terbalik dengan respons timenya.

$$
\text{performance}_x = \frac{1}{\text{time}_x}
$$

Saat kita bilang bahwa mesin $X$ n kali lebih cepat dari mesin $Y$, maka percepatan n-nya dihitung dengan:

$$
\text{Speedup} = \frac{\text{time}_y}{\text{time}_x} = \frac{\text{performance}_x}{\text{performance}_y}
$$

### Computing Time

Ada banyak cara untuk menghitung waktu. Namun, waktu mana yang harus kita ukur.

- **Elapsed time**
  - waktu dari job dimasukkan sampai keluar hasilnya.
  - Mengukurnya mudah.
  - akan menghitung semuanya termasuk waktu untuk mengakses disk, memori, input output, dan lainnya.
  - Tidak terlalu baik untuk perbandingan, terutama yang ingin kita bandingkan ialah kecepatan prosesor.

- **CPU Time**
  - tidak termasuk input output atau waktu yang dihabiskan untuk menjalankan proses program lain.
  - Kita ketahui bahwa CPU tentu saja melakukan beberapa tugas dalam suatu waktu, sehingga pasti ada beberapa program yang diproses di dalam CPU secara bersamaan.
  - Terdiri dari system time (berkaitan dengan OS, dan koordinasi semua sistem) dan user time.

- Fokus kita ialah **User CPU time**
  - Waktu yang dihabiskan untuk memproses baris-baris perintah di dalam program. Di POK yang difokuskan ialah User CPU time.

Bagaimana cara mengukur **User CPU time**?

Saat kita membeli komputer salah satu informasi yang kita punya ialah clock cycles. Untuk melaporkan waktu eksekusi, biasanya tidak menggunakan detik, tapi clock cycles, yaitu basis satuan waktu di dalam mesin.

$$
\frac{\text{detik}}{\text{program}} = \frac{\text{cycles}}{\text{program}} × \frac{\text{detik}}{\text{cycle}}
$$

Komponen pertama yang dibutuhkan ialah kita harus tau, berapa clock cycle yang diperlukan untuk menjalankan program kita dari awal sampai akhir.

Kemudian, komponen keduanya kita harus tau Cycle time, atau satu cyclenya berapa detik (Periode atau Seconds/Cycle), atau satu detik berapa cycle (Frekuensi atau Cycles/Second). Jangan terbalik-balik! 😄

Cycle time (Cycle period atau clock period) ialah jarak waktu di antara dua rising edge berurutan dalam detik.

Clock rate (Clock frequency) ialah banyaknya cycle dalam $1$ detik. Satuannya Herts, dan $1$ Hertz artinya terdapat $1$ Cycle dalam $1$ detik.

Contoh : $200$ MHz clock

$$
1 \text{ Kilo (K)} = 10^3\\
1 \text{ Mega (M)} = 10^6\\
1 \text{ Giga (G)} = 10^9\\
1 \text{ Tera (T)} = 10^{12}
$$

Maka setiap detiknya ada $200 × 10^6$ clock/detik-nya pada mesin tersebut.

Jadi misal ingin dihitung cycle time atau clock period, artinya **berapa detik lamanya sebuah clock**.

$$
\begin{aligned}
&= \frac{1\text{ detik}}{200 \times 10^6}\\
&= \frac{1}{2}.\frac{1}{10^8}\\
&= 0.5 × 10^{-8}\\ 
&= 5 × 10^{-9} \text{ s}\\
&= 5 \text{ ns}
\end{aligned}
$$

$$
10^{-3} = 1 \text{ mili (m)}\\
10^{-6} = 1 \text{ micro (µ)}\\
10^{-9} = 1 \text{ nano (n)}\\
10^{-12} = 1 \text{ pico (p)}\\
$$

Saat ingin mempercepat kecepatan CPU User Time, maka seconds/program harus diturunkan dari yang lama. Sehingga, mesin berjalan lebih cepat.

$$
\frac{\text{detik}}{\text{program}} = \frac{\text{cycles}}{\text{program}} × \frac{\text{detik}}{\text{cycle}}
$$

Pada komponen pertama, agar lebih cepat dapat diturunkan. Maksudnya ialah kita membuat program yang lebih efektif. Kita cari algoritma atau compiler yang lebih baik. Program saya harus bisa dilaksanakan dalam instruksi yang lebih sedikit.

Pada komponen kedua, agar lebih cepat harus diturunkan pula. Maksudnya ialah kita menurunkan clock period dari CPU kita, atau meningkatkan clock frequency-nya.

### Cycles per Instruction

Setiap instruksi pada suatu program akan dilaksanakan mengikuti cycle. Ada nilai CPI yang menunjukkan untuk setiap instruksinya membutuhkan berapa cycle.

Perhatikan bahwa instruksi yang berbeda membutuhkan jumlah cycle yang berbeda pula.

Bagaimana cara menghitung CPI:

- Apakah jumlah cycle sama dengan jumlah instruksi? Tidak.
- Apakah jumlah cycle sebanding/proporsional secara linear dengan jumlah instruksi? Tidak.

Penyebabnya karena setiap instruksi memerlukan waktu yang berbeda-beda untuk melaksanakan instruksi.

- Saat mengalikan, sama saja dengan menjumlahkan berulang. Tentu perkalian membutuhkan waktu yang lebih banyak dari penjumlahan.
- Mengakses bilangan floating point tentu akan lebih banyak dan lama operasinya dibandingkan integer
- Mengakses main memory akan lebih lama dari mengakses register.

Untuk menghitung Average cycle per instruction, kita harus menghitung setiap komponennya.

CPI = (CPU time × Clock rate) / Instruction count
= Clock cycles / Instruction count

CPU time = Seconds/Program
= Instructions/Program × Cycles/Instruction × Seconds/Cycle

$$
\text{CPI} = \sum_{k = 1}^n \text{CPI}_k × F_k
$$

dengan

$$
F_k = \frac{I_k}{\sum{I_k}}
$$
$$
\begin{aligned}
I_k &= \text{Banyak/Frekuensi Instruksi}\\
∑ I_k &= \text{Total Instruksi}
\end{aligned}
$$

### Contoh 1

Suatu program berjalan dengan waktu 10 detik pada komputer A, dengan frekensi clock $400 \text{ MHz}$. Ingin didesain mesin baru yang bisa menjalankan program ini dalam 6 detik. Namun mesin B membutuhkan 1.2 kali clock cycles lebih banyak dari program A untuk program yang sama.

$$
\begin{aligned}
\frac{\text{detik}}{\text{program}} &= \frac{\text{cycles}}{\text{program}} × \frac{\text{detik}}{\text{cycle}}\\
10 \text{ detik} &= \frac{\text{cycles}}{\text{program}} × \frac{1}{400 ⋅ 10^6 \text{ Hz}}\\
4 ⋅ 10^9  &= \frac{\text{cycles}}{\text{program}}
\end{aligned}
$$

Pada mesin B, akan dibutuhkan 1.2 kali lebih banyak cycle, dan ingin dibuat berjalan dalam 6 detik. Maka,

$$
\begin{aligned}
\frac{\text{detik}}{\text{program}} &= \frac{\text{cycles}}{\text{program}} × \frac{\text{detik}}{\text{cycle}}\\
6 \text{ detik} &= 4 ⋅ 10^9 ⋅ 1.2 × \frac{\text{detik}}{\text{cycle}}\\
\frac{6}{4.8 ⋅ 10^9} &= \frac{\text{detik}}{\text{cycle}}\\
\frac{4.8 ⋅ 10^9}{6} &= \frac{\text{cycle}}{\text{detik}}\\
800 \text{ MHz} &= \frac{\text{cycle}}{\text{detik}}\\
\end{aligned}
$$

### Contoh 2

Misalkan kita ingin memilih antara dua buah kode, mana yang lebih optimal. Kode-kode tersebut ada yang menggunakan Class A, B, dan C yang masing-masing membutuhkan 1, 2, dan 3 cycles secara berturut-turut.

Program pertama ada 5 instruksi: 2 A, 1 B, dan 2 C.

Program kedua ada 6 instruksi: 4 A, 1 B, dan 1 C.

Kode mana yang lebih cepat? Seberapa cepat dari yang lain?
Berapa CPI untuk setiap kodenya?

#### Analisis Kode Pertama

- 2 Tipe A = 2 Cycles
- 1 Tipe B = 2 Cycles
- 2 Tipe C = 6 Cycles
- Jumlah clock = 10 Cycles

- CPI = 10 Cycles/5 Instruksi = 2 

#### Analisis Kode Kedua

- 4 Tipe A = 4 Cycles
- 1 Tipe B = 2 Cycles
- 1 Tipe C = 3 Cycles
- Jumlah clock = 9 Cycles
- CPI = 9 Cycles/6 Instruksi = 1.5

Karena dilakukan proses pada komputer yang sama, maka clock rate atau frekuensinya sama.

Kode 2, tentu lebih cepat $\frac{10}{9}$ kali dari Kode 1.

### Contoh 3

Misalkan ada dua mesin A dan B. Mesin A memiliki clock cycle time 10 ns, dan CPI 2.0. Mesin B memiliki clock cycle 20 ns, dan CPI 1.2.

Yang mana mesin yang lebih cepat? Seberapa cepat?

Saat jumlah instruksinya sama, atau instruksi untuk programnya sama, waktu CPU dapat dihitung dengan rumus.

$$
\frac{\text{seconds}}{\text{program}} = \frac{\text{instructions}}{\text{program}} ⋅ \frac{\text{cycles}}{\text{instruction}} ⋅ \frac{\text{seconds}}{\text{cycle}}
$$

Karena ISA yang sama, maka instructions per programnya secara implisit dianggap sama.

$$
\text{Mesin A} = N ⋅ 2.0 ⋅ 10 \text{ ns} = 20N \text{ ns}\\
\text{Mesin B} = N ⋅ 1.2 ⋅ 20 \text{ ns} = 24N \text{ ns}\\
$$

Mesin A, lebih cepat 24/20 kali, atau 1.2 kali dari Mesin B.

### Contoh 4

Panjang banget, hehehe. Bisa dilihat di video 3 contohnya.

Perhatikan bahwa semakin kecil CPI, maka semakin cepat mesinnya.

### Benchmark

Benchmark adalah sesuatu yang sangat bermanfaat. Dengan adanya benchmark kita dapat lebih memahami brosur-brosur komputer yang biasa kita dapatkan saat ingin membeli laptop baru. 📰

Untuk menghitung performance suatu mesin, banyak yang berlomba-lomba bilang bahwa performance mesinnya lebih bagus. Namun tidak ada standar khususnya. Sehingga diadakanlah sistem benchmarking ini. 👍

Benchmarking ialah proses menentukan program untuk mengevaluasi performance dari suatu mesin. Gunanya ialah untuk menghitung atau mengukur, sebagai standar. Jadi kita membandingkan apple to apple. 🍎

Mencari suatu kondisi yang standar dan sama untuk setiap mesin. Program yang dipilih diharapkan dapat menggambarkan workload yang akan diberi.

Benchmarks ialah program-program yang digunakan untuk mengukur performance.

Saat ingin membuat benchmark, ada beberapa kemungkinan yang digunakan sebagai benchmark

#### Actual Target Workload

Yaitu workload asli, yang akan diberikan.

Keuntungan:

- Sangat representatif.

Kerugian:

- Terlalu spesifik, tidak bisa saling mencocokkan dengan versi lain.
- Tidak portable, tidak ramah pindah platform.
- Mengukurnya susah, karena datanya sulit diduplikasi dengan data asli.
- Susah mengidentifikasi problem/flaws saat melakukan benchmark.

#### Full Application Benchmarks

Menggunakan aplikasi benchmarks.

Keuntungan:

- Portable, bisa dipakai antar platform.
- Banyak dipakai orang, user basenya besar.
- Improvementnya berguna untuk banyak orang.

Kerugian:

- Kurang representatif.

#### Small "Kernel" Benchmarks

Saya kebingungan arti kernel apa ☹️

> A kernel is the foundational layer of an operating system (OS). It functions at a basic level, communicating with hardware and managing resources, such as RAM and the CPU.

Kalau saya baca, itu artinya semacam core atau inti dari suatu sistem operasi. Saya kira semacam kacang-kacangan atau makanan yang berhubungan dengan jagung. 🌽

Keuntungan:

- Mudah dijalankan

Kerugian:

- Mudah mengecoh, bisa kurang akurat.

#### Microbenchmarks

Keuntungan:

- Bisa mengetahui kemampuan atau potensi tertinggi, serta tempat dimana proses terlambat terjadi, yaitu bottlenecksnya.

Kerugian:

- Puncak performance biasanya jauh dari aplikasi, belum tentu cocok saat diterapkan.

### SPEC '95

SPEC (Systems Performance Evaluation Cooperative)

SPEC '95 sudah deprecated dan retired, sekarang ada evaluation yang baru, sekitar tahun 2017-2019.

Perusahaan-perusahaan sudah menyetujui untuk menggunakan input dan program tertentu untuk benchmark.

Pada SPEC '95, terdapat 18 aplikasi (dengan input) yang mencerminkan atau menggambarkan workload komputasi, kira-kira dipakai untuk apa.

Ada 8 untuk operasi integer:

- go, m88ksim, gcc, compress, li, ijpeg, perl, vortex.

Ada 10 untuk operasi floating-point yang berat:

- tomcatv, swim, su2cor, hydro2d, mgrid, applu, turb3d, apsi, fppp, wave5.

Compile flag yang digunakan juga sudah standar, tidak boleh ada yang dioptimisasi atau dinerf.

Yang spesifik atau khusus dibuang, akan digunakan program yang umum. SPEC '95 masih bisa diharass, diabuse, bug dari intel.

SPEC ini cukup memberi gambaran untuk kinerja suatu mesin.

Pada suatu ISA, peningkatan performance dari CPU bisa dari 3 sumber:

- Clock rate atau frekuensinya dinaikkan.
- Bisa diimprove organisasinya, sehingga instruksinya lebih sedikit, atau CPI nya lebih sedikit.
- Compiler atau penyusunan ke bahasa mesinnya di-enhance agar menurunkan jumlah instruksi rumit menjadi lebih sederhana, sehingga menurunkan CPI.

Pada frekuensi yang sama, Pentium Pro 1.4 kali lebih cepat untuk komputasi integer dan 1.7 kali lebih cepat untuk floating point dari Pentium. Lebih cepat karena adanya pipelining, sistem memori. 

### Amdahl's Law

Ada kesalahan, berdasarkan hukum Ahmdahl. yaitu mengharapkan improvement dari salah satu aspek komputer kita, maka akan proporsional pada seluruh performa mesin.

Misalnya ada suatu mesin yang menjalankan program selama 100 detik. Kemudian ada komponen multiplikasi yang menghabiskan 80 detik. Apabila kita ingin programnya 4 kali lebih cepat, berapakah harus ditingkatkan speedup multiplikasinya?

Untuk mempercepatnya 4 kali, kita harus lihat komponennya. Komponen non perkalian memakan waktu 20 detik, komponen perkalian 80 detik. Saya ingin mempercepat prosesnya 4 kali lebih cepat. Waktunya akan terpotong, hingga 1/4 kali. Maka harus berjalan selama 25 detik.

Perhatikan bahwa pada bagian non perkalian tetap menghabiskan 20 detik, maka komponen perkalian harus meningkat dari 80 detik menjadi 5 detik. Kita harus mempercepat 16 kali.

Contoh ini memberikan gambaran, apabila kita meningkatkan komponen perkalian sebanyak 16 kali, maka program secara keseluruhan hanya akan meningkat sebanyak 4 kali. Meskipun proses perkalian sangat cepat, namun Amdahl's Law berlaku disini.

Misal saya ingin meningkatkan kecepatan mesin 5 kali lipat. Maka yang tadinya 100 detik, harus menjadi 20 detik. Sekarang, maka proses perkalian harus "Super speed", yaitu 0 detik(?) Mungkinkah? tentu tidak!

Maka hal ini tidak feasible dan tidak mungkin dilakukan. Dalam merancang sesuatu, tentu ada batas dan limitasi.

Performance overall suatu program tentu dibatasi oleh komponen yang tidak dipercepat. Korelasinya, ialah saat ingin meningkatkan suatu komponen, kita harus perhatikan komponen yang paling banyak, sering atau berkontribusi paling besar terhadap waktu jalannya program.

Saat meningkatkan yang "boros", maka overall akan meningkat lebih baik.

Misalnya komponen "penambahan" atau adder, yang sering digunakan atau dipanggil dalam berbagai instruksi, dapat ditingkatkan karena digunakan dalam banyak komponen.

### Contoh 5

Misalkan kita ingin meningkatkan operasi floating-point 5 kali lebih cepat. Saat belum ada peningkatan, dibutuhkan 12 detik untuk proses programnya, separuhnya dipakai dalam operasi floating point. Yaitu, sebanyak 6 detik.

Saat dipercepat 5 kali lipat, maka waktu operasi floating point hanya akan menghabiskan 6/5 = 1.2 detik!, komponen non-floating-point akan menghabiskan 6 detik. Sehingga total program akan menjadi 7.2 detik. Sehingga total percepatannya ialah 12/7.2 = 1.67 kali.

### Contoh 6

Misalnya Anda tidak sudi meningkatkan dengan lelah operasi floating-point. Anda ingin mencari benchmark yang floating-pointnya lebih banyak, yang komposisinya mungkin tidak 50:50 seperti tadi. Kita ingin percepatan totalnya tiga kali lipat. Misalnya ada benchmark yang berjalan selama 100 detik. Maka berapa komposisinya sehingga target kita tercapai?

Mari kita simak. Kita misalkan bobot floating point dengan $x$. Agar total dipercepat tiga kali lipat, maka program harus berjalan secepat $100/3$ detik, atau $33.33$ detik

Percepatan sebanyak 5 kali lipat pada komponen floating-point akan menjadikan waktunya menjadi $\frac{x}{5}$ detik. Perhatikan bahwa komponen non-floating point akan menghabiskan waktu sebanyak $100-x$ detik.

dapat dibuat persamaan:

$$
\begin{aligned}
\frac{x}{5}+(100-x) &= \frac{100}{3}\\
\frac{x}{5}+\frac{500-5x}{5} &= \frac{100}{3}\\
3\cdot(500-4x) &= 5\cdot100\\
1500-12x &= 500\\
12x &= 1000\\
x &= \frac{1000}{12}\\
&= 83.333\\
\end{aligned}
$$

Jadi, komponen floating point pada program tersebut harus sebanyak 83.333 detik!

Sehingga saat kita membaca brosur atau benchmark, kita harus perhatikan dia menggunakan benchmark apa, sehingga kita bisa melihat dia bagusnya dimana. Saat kita melihat "Oh ini program saya kencang!" tapi kita harus perhatikan ini menggunakan benchmark yang mana, komponennya apa, dan seberapa besar komponen tersebut pada benchmark yang ada.
## MIPS Instruction Set Architecture

Kita akan menggunakan mesin MIPS, bahasanya SPIM.
Bahasa Assembly merupakan bahasa low-level. Kita harus menggunakan perangkat yang ada di komputer secara spesifik, seperti register, memory.

Pada high-level language, kebanyakan informasinya sudah di-encapsulate. Bahasa assembly digunakan untuk proyek peluncuran manusia ke bulan.


### ISA

Instruction Set Architecture ialah sekumpulan instruksi yang dapat dilakukan oleh hardware. Instruksi yang lebih kompleks dibangun dari instruksi dasar. Sebelum merancang komputer ialah ISA atau instruksi apa yang dapat dikerjakan dari hardware-hardware tersebut.

ISA bertindak sebagai abstraksi pada interface antara hardware dan low-level software.

Dari sisi software, semua instruksi pada software harus bisa diterjemahkan menjadi instruksi yang lebih sederhana yang terdapat pada instruction set architecture.

Dalam mempelajari suatu mesin, kita harus mengetahui instruksi apa yang tersedia dan dapat dilakukan mesin itu. Dengan instruksi tersebut, kita dapat membuat instruksi yang lebih kompleks.

ISA biasanya tidak banyak jenisnya, tapi cara eksekusinya yang berbeda-beda. Implementasinya berbeda-beda tergantung pada hardware designernya. Misalnya Addernya ada yang ripple carry adder, carry ahead adder. Di ISA tidak dispesifikasi implementasi dari adder, namun dapat diketahui bahwa Add dapat dilakukan di ISA tersebut.

ISA ditentukan oleh

- Organisasi dari penyimpanan data dan program
- Tipe data dan data struktur, cara menyimpannya dan representasinya.
- Format instruksi, ada berapa instruksi yang diimplement
- Ada instruksi apa saja, operandnya apa saja
- Addresing, alamat, bagaimana mengakses program dan data.
- Kondisi pengecualian.

MIPS memberikan kita basic tentang ISA-ISA yang ada. Jadi saat pindah platform akan lebih terbiasa.

Bagaimana komputer melaksanakan setiap instruksi yang ia terima?

- Instruction Fetch = Mengambil instruksi
- Instruction Decode = Mencari tipe instruksi yang tadi di fetch, mana yang operand, operator.
- Operand Fetch = Menyiapkan operand yang akan diambil, operasi apa
- Execute = Menjalankan instruksi
- Result Store = Menyimpan hasilnya, lokasi operandnya dimana, lokasi hasilnya dimana.
- Next Instruction = Instruksi apa yang nantinya harus dijalankan? Tapi dapat saja diganti dengan branches, atau conditional.

Setiap instruksi yang akan dilaksanakan akan mengikuti 6 tahapan ini secara berurutan dan biasanya dalam satu clock cycle.

Instruction set ialah bahasa yang dimengerti oleh mesin. Bahasanya lebih primitif dari high-level, tidak ada yang seperti sorting aneh-aneh. Tapi kita bisa membuatnya.

Instruksi yang tersedia sederhana, kita harus berpikir untuk membangun instruksi yang lebih kompleks.

MIPS biasanya dipakai perusahaan-perusahaan game juga.

### Assembly Language

Assembly language sebetulnya satu level di atas machine language. Bahasa machine language hanya terdiri dari satu dan nol, dan hanya dimengerti oleh mesin. Kita tidak mungkin ngoding menggunakan binary.

Oleh karena itu kita belajar di bahasa yang lebih tinggi satu layer, yaitu Assembly Language. Ada map yang bisa menerjemahkan dari Assembly ke bahasa mesin, tapi masih readable oleh kita.

Assembly ialah simbolis dari machine code yang masih kita baca. Assembly human readable, tapi belum bisa dimengerti mesin. Ada yang namanya assembler. Dari assembly language ke bahasa mesin.

Di Assembly language ada pseudo instruction. Misalnya fungsi move, yang sebenarnya fungsi add, dari zero dan operand lain.

Kita usahakan sedikit mungkin menggunakan pseudo instruction

### RISC vs CISC

CISC ialah Complex Instruction set computer. Suatu instruksi akan melakukan operasi yang kompleks, ukuran program akan menjadi lebih kecil. Namun karena implementasinya yang kompleks, optimisasi hardware akan sulit dilakukan, namun lebih hemat memori.

Mesin VAX bisa mengalikan polinomial, yang batasnya tidak ditentukan. Jelas sekali bahwa instruksi ini sangat kompleks. Untuk melakukan konvolusi membutuhkan banyak tahap.

RISC ialah Reduced Instruction Set Computer. Optimisasi hardware sangat memungkinkan karena instruksinya kecil dan sederhana. Pada RISC, programmer akan mendapat beban banyak untuk mengimplementasi sesuatu yang kompleks seperti perkalian polinomial dari instruksi sederhana yang ada.

Pentium 4 ISA nya CISC tapi yang menyusun complex instructionnya RISC.

### MIPS Assembly Language

Setiap instruksi hanya akan mengeksekusi perintah sederhana. Setiap barisnya paling banyak satu operasi

Pada Assembly isinya hanya instruksi sederhana seperti kali bagi tambah kurang.

Dianjurkan untuk melakukan komentar menggunakan `#` karena sangat sulit dibaca dan tidak mereturn error pada MIPS.

### Arithmetic: Addition

```assembly
add $s0, $s1, $s2
```

Pada operasi tersebut, ada 3 operand, $s1 ialah source 1, $s2 ialah source 2. $s0 ialah destination variablenya, dimana hasil akan diletakkan.

Semua operand harus berada di Register. Limited amountnya. Di MIPS ada 32 Registers. 

Register dapat diakses dengan nomor register ($0, $1, .. , $31) tapi lebih baik diakses dengan nama agar lebih diingat.

- 0, $zero menyimpan nilai konstan 0
- 2-3 $v0-$v1 menyimpan hasil dan result evaluasi ekspresi, kepanjangannya value
- 4-7 $a0-$a3 Menyimpan argumen yang akan dijalankan oleh syscall, kepanjangannya arguments
  8-15 $t0-$t7 Menyimpan nilai-nilai temporaries, kepanjangannya temporary
- 16-23 $s0-$s7 biasanya untuk variabel program yang tidak diganti atau mau disimpan dan dipakai nantinya, kepanjangannya store
- 24-25 $t8-$t9 temporary juga
- 28 $gp Pointer global
- 29 $sp Pointer stack
- 30 $fp Pointer frame
- 31 $ra return address

$at, register 1 reserved untuk assembler, pseudo instructions biasanya.
$k0-$k1 register 26-27  reserved untuk sistem operasi.

Referensi:

[Cheatsheet](http://homepage.divms.uiowa.edu/~ghosh/1-28-10.pdf)

### Arithmetic: Subtraction

```assembly
sub $s0, $s1, $s2
```

artinya $s0 = $s1 - $s2

Jangan tertukar karena pengurangan tidak komutatif

### Complex Statements

Bagaimana cara melakukan a = b + c - d
Suatu instruksi paling banyak hanya bisa mengerjakan dua operand. Di assembly, akan dipecah menggunakan temporary pengerjaannya.

```assembly
add $t0, $s0, $s2
sub $s0, $t0, $s3
```

Suatu baris pada bahasa C dapat menjadi pecahan-pecahan instruksi di assembly

### Constant/Immediate Operands

- Bagaimana cara kita menambahkan suatu nilai di luar variabel?
- Misal kita ingin menambahkan bilangan konstan.
- Gunakan add immediate (addi)

```assembly
addi $s0, $s0, 4
```

akan melakukan `a += 4`. Mirip dengan add, namun source2 nya constant. Kenapa MIPS tidak menyediakan `subi`? Karena operand kedua boleh negatif.

Nantinya bilangan negatif akan ditulis dalam twos complement. Ada pula register

### Register Zero

Angka 0 konstan sering sekali muncul dalam kode. Misalnya ingin melakukan assignment

```assembly
add $s0, $s1, $zero
```

Atau ingin melakukan perbandingan. Perhatikan bahwa $zero meskipun dimodifikasi tetap bernilai 0 dan pointless.

Referensi:

[https://stackoverflow.com/questions/24646101/how-does-a-zero-register-improve-performance](https://stackoverflow.com/questions/24646101/how-does-a-zero-register-improve-performance)

### Logical Operations

Perhatikan bahwa angka disimpan dalam bit biner. Sehingga kita bisa melakukan operasi logika di luar operasi aritmatika.

- `sll`, Shift left logical
- `srl`, Shift right logical
- `and` dan `andi`, and dan and immediate
- `or` dan `ori`, or dan or immediate
- `nor`, untuk melakukan komplemen.

### Shift instructions

Dapat bertindak sebagai array of booleans, menjadi bitmask. Shift left dan right sebanyak \(i\) juga dapat digunakan untuk perkalian dan pembagian bilangan bulat dengan \(2^i\).

Shifting lebih cepat dari operasi pembagian dan perkalian. Sehingga untuk melakukan operasi langsung dengan bilangan dua pangkat, lebih baik menggunakan shift.

### Bitwise AND Instruction

Akan melakukan operasi AND pada seluruh bitnya. Bisa dilakukan untuk membuat mask, hanya menunjukkan yang keduanya bernilai 1 dari 2 bilangan. Perhatikan bahwa AND bersifat komutatif. Gunanya untuk mematikan bit pada posisi-posisi tertentu.

Misalnya ingin melakukan modulo terhadap bilangan Sangat berguna untuk melakukan modulo terhadap bilangan \(2^i\), yaitu tinggal melakukan `andi` terhadap mask \(2^i-1\) nya.

Perhatikan bahwa immediate juga dapat ditulis dalam hexadecimal.

### Bitwise OR Instruction

Akan menghidupkan bit pada posisi-posisi tertentu. Kurang lebih syntaxnxya sama dengan yang lain.

### Bitwise NOR Instruction

Untuk mentoggle operandnya. Saat diberikan 0, maka akan dikomplemen. Jadi A nor 0 akan mengembalikan ~A. Apabila diberikan 1, akan mematikan semua bitnya. Jadi A nor 0xFFFFFFFF akan mengembalikan 0.

### MIPS

MIPS ialah typicalnya arsitektur RISC, atau instruction set yang sederhana. MIPS dikenal dengan arsitektur load-storenya, semua operasi hanya bisa dilakukan dalam register. Data yang ada di memori harus dibawa ke register dengan instruksi load. Jika ingin disimpan dalam memori, harus melalui perintah store. Inilah yang disebut arsitektur load-store. Address untuk mengakses memorinya 32 bit, dan registernya ada 32.

### Byte vs Word Addressing

Addressing itu layaknya menyimpan indeks. Jika ada 2 bit untuk address, berarti kita bisa memetakan 2^2 = 4 posisi berbeda.

Jika memiliki 32 Bit, maka ada \(2^32\) word berbeda yang bisa diakses. Lebar dari setiap address adalah banyak data kita. Ini disebut byte addressing.

Misal setiap datanya lebarnya 1 byte = 8 bits. Maka akan ada 2^35 bits atau 2^32 Bytes memorinya. yaitu sekitar 2^2 . 2^30 atau 4 GB.

Word Addressing ialah saat setiap address mengakses 1 word, 1 word bisa beragam, biasanya 4 Bytes.

Misalnya total datanya ada 4 GB dan 1 word ada 4 Bytes, maka addressnya akan jadi 30 bit saja. Setiap addressnya akan langsung mengakses paketan-paketan data. Datanya sama. Tapi dibuat seperti row dan column, dan saat query langsung diambil satu row.

Perhatikan jika pada word addressing nantinya memorynya loncat loncat, sebanyak panjang word.

### Memory Access Instructions

Misalnya setiap element array isinya satu word.

Misal pada C ingin dibuat A[7] = h + A[10].

Dalam array, base address ialah alamat indeks pertama / [0], dari array. Jika kita mengetahui base addressnya, kita akan mengetahui seluruh alamat arraynya, karena disimpan berurutan.

Alamat A[i] = Base address + i*panjang word.

Format untuk mengaksesnya ialah misal basenya $s3, dan satu word 4 bytes.

```
displacement(Base Register)
A[10] = 40($s3)
```

Karena proses hanya bisa dilakukan dalam register, maka harus load dulu wordnya ke dalam register, baru bisa diproses.

Operasi awal tadi dapat ditulis dalam

```assembly
lw $t0, 40($s3)
add $t0, $t0, $s2
sw $t0, 28($s3)
```

### Other Load/Store Instruction

Selain load word, ada pula load byte dan store byte. Perhatikan bahwa yang diambil hanya satu byte.

```assembly
lb $t1, 12($s3)
sb $t2, 13($s3)
```

Ada juga unaligned load word(ulw) dan unaligned store word(usw), satu wordnya bisa dimana saja. Instruction ini ialah pseudo instruction.

Ada pula lh dan sh untuk menyimpan setengah halfword atau setengahnya. Biasanya selalu diambil lo nya.

Ada juga lwl, lwr, swl, swr, yaitu load word left, load word right, store word left, store word right.

misal ada fungsi
```C
void swap (int v[], int k){
  int temp = v[k];
  v[k] = v[k+1];
  v[k+1] = temp;
}
```

swap(A, 10) akan menukar isi indeks 10 dan 11.


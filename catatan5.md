### MIPS Instructions Classification

Instruksi ada tiga macam

#### R-Format (Register Format)

- add, sub, and, or, nor, slt yang membutuhkan 2 source registers dan 1 destination register
- srl dan sll untuk shift right dan left juga termasuk ke sini.

#### I-Format (Immediate format)

Ada tiga kategori:

- addi, subi, andi, ori, slti (Semua instruksi yang ada operasi immediate di belakangnya)
- lw, sw (Load dan store)
- heq, bne (Branching equal dan not equal)

yang membutuhkan 1 source register, 1 immediate register dan 1 destination register. Sepintas terlihat sama dengan R, tapi sebenarnya immediate ialah yang paling kompleks karena diimplement berbeda.

#### J-Format (Jump Format)

- instruksi j untuk melompat, hanya membutuhkan satu operand

### Registers

Nomor register yang sesungguhnya yang akan dimasukkan ke instruksi dalam mesin, ada 32 Registers pada MIPS. dibutuhkan 5 bit untuk merepresentasikan setiap register.

### R-Format

Instruksi MIPS panjangnya 32 bit. Dibagi dalam chunk $[6, 5, 5, 5, 5, 6]$

Untuk kesederhanaan, setiap field ada namanya
[opcode, rs, rt, rd, shamt, funct]

opcode: Kode Instruksi
rs: Source pertama
rt: Source kedua (Atau destination saat digunakan operasi tipe i)
rd: Destination
shamt: Shift amount (Ada 5 bit yang bisa kita shift, maksimum hanya shift 32 kali)
funct: Untuk tipe R, semua opcodenya 0, kemudian beda di funct.

- Suatu bagian fieldnya itu ditulis sebagai 5-bit atau 6-bit bilangan bulat tak bertanda, bukan bilangan bulat 32.

- Field 5 bit bisa direpresentasikan dari 0-31
- Field 6 bit bisa direpresentasikan dari 0-63

opcode:

- Menspesifikasi suatu instruksi
- Bernilai 0 semua untuk instruksi format R

funct:

- Menspesifikasi funct yang dikombinasikan dengan opcode.
  
rs: (Source Register)

- Menyatakan operand pertama.

rt: (Target Register)

- Menyatakan operand kedua, namanya misleading.

rd: (Destination Register)

- Menyatakan register yang menjadi tujuan, yang menjadikan hasil dari komputasi.

shamt:

- Terdiri dari 5 bit, seberapa banyak jumlah instruksi shift harus dilakukan. Melakukan shift lebih dari 31 kali untuk word berukuran 32 bit tidak berguna, jadi hanya 5 bit saja karena bisa merepresentasikan 0-31. Bernilai 0, kecuali saat instruksi shift.

#### Contoh 1

```assembly
add $8, $9, $10
add $t0, $t1, $t2
```

Maka dapat dibuat fieldsnya:
opcode: 0
rs: 9 = 0b1001
rt: 10 = 0b1010
rd: 8 = 0b1000
shamt: 0
function: 0x20 = 0b100000

000000 01001 01010 01000 00000 100000

0000 0001 0010 1010 0100 0000 0010 0000

Hexadecimal representation

0x012A4020

#### Contoh 2

Perhatikan bahwa Arithmetic Logic Unit dapat menerima dari A dan B, namun shifter hanya menerima dari B, atau source nomor 2.

```assembly
sll $8, $9, 4
sll $t0, $t1, 4
```

Maka dapat dibuat fieldsnya
opcode: 0
rs: 0 = 0b0
rt: 9 = 0b1001
rd: 8 = 0b1000
shamt: 4 = 0b100
function: 0

000000 00000 01001 01000 00100 000000
0000 0000 0000 1001 0100 0001 0000 0000

Hexadecimal Representation
0x00094100
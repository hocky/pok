Selamat sore, Tim Dosen POK Gasal 2020/2021, saya sempat membuat ringkasan untuk materi minggu pertama ini.

## Organisasi dan Arsitektur Komputer

Organisasi komputer ialah titik pandang dari ilmuwan elektronik terhadap sistem komputer, membuat hardware komputer lebih baik. 👍

Arsitektur komputer lebih kepada sudut pandang programer, untuk kepentingan program kita. 💻

Istilah ini sering dipertukarkan, definisinya kurang tegas.

Kita sebagai ilmuwan komputer harus tau arsitektur komputer, tau bagaimana program itu berjalan di dalam komputer.

Kita bukan membahas lebih ke komponennya, tapi ke bagaimana instruksi pada komputer itu bekerja di dalamnya.

Biasanya kita menulis program dalam bahasa tingkat tinggi yang lebih bisa dipahami manusia. Komputer tidak mengerti high level language. Compiler akan menerjemahkan high-level language ke assembly.

Assembly merupakan program yang lebih low level, memakai langsung komponen-komponen yang ada di processor. Kita sudah menyiapkan parameter yang ingin diletakkan pada register. 

Di high level, kita masih menggunakan variabel, kita tidak peduli dengan registernya. Pada assembly, kita bermain dengan komponen register.

Assembler istilahnya compiler yang menerjemahkan dari assembly ke bahasa mesin. Sesudah dalam bentuk binary, baru ada artinya bagi komputer, kemudian dapat dimengerti dan dijalankan.

Assembly itu bahasa pemrograman yang hanya menyediakan perintah-perintah primitif. Tidak seperti C++, python yang bisa mengakarkan, memangkatkan, dsb.

- Kita menulis di high level
- Compiler merubah menjadi assembly, bahasa yang lebih primitif
- Assembler merubah menjadi binary, bahasa mesin.

Pada beberapa bahasa seperti PHP, Ruby, Python, JavaScript, ada istilah *interpreter*, yaitu untuk setiap baris per baris bahasa high-level pemrogramannya, diterjemahkan ke bahasa komputer. Misalnya di python, kita bisa memberikan command satu per satu tanpa harus dijalankan sekaligus seperti di C++ atau C. 😢 Setelah browsing saya baru tahu hal ini. Cukup menjebak juga ternyata soal kuis 1. ☹️

## von Neumann Architecture

Pada arsitektur komputer ini, ada beberapa komponen.

- Processor (Control, Datapath, Registers)
- Memory (Cache)
- Devices (Input, Output)

Di antara komponen tersebut, terdapat bus, yang menghubungkannya.

## Components of Computer 💻

- Memory: Menyimpan program dan data
- Input: Menerima data, misalnya keyboard dan mouse
- Datapath: Membaca data dari memori, diproses dan diolah, kemudian hasil perhitungannya disimpan kembali di memori.
- Control: Membuat signal yang diperlukan datapath agar dapat menjalankan instruksi sesuai yang harus dilaksanakan.
- Output: Menampilkan hasil perhitungan.

## Datapath 👨‍💻

Datapath itu kumpulan unit fungsional, di dalamnya ada ALU (Arithmetic Logic Units) yang melakukan pemrosesan data. Bersama Control unit, terbentuklah CPU (Central Processing unit).

Ada apa di dalam Datapath?

- Empat buah Parallel-Load Register (Sudah dipelajari di PSD) Gunanya untuk menyimpan data, dan tentunya bisa Load dari luar secara bersamaan sesuai clock cyclenya.
- Dua buah Multiplexer a dan b, untuk **memilih** data mana dari register yang akan dimasukkan ke ALU.
- Decoder, yang menentukan hasil pemrosesan akan diletakkan di register mana.
- Multiplexer B untuk input dari luar, yang dapat menentukan input yang akan masuk ke Function unitnya dari luar atau dari MUX b. Dua multiplexer di dalam Register File akan terhubung ke MUX B ini.
- Bus A, yaitu wiring yang menghubungkan MUX a dengan ALU, serta MUX B dengan ALU.
- ALU dan Shifter serta MUX F untuk memilih antara ALU atau shifter.
- MUX D untuk memilih MUX F atau konstanta dari luar.

## Buses 🚌

Bus itu yang menghubungkan komponen-komponen dari komputer. Jenisnya ada data bus, control bus, dan address bus. Intuitifnya, bus ini semacam kabel, atau wire yang di dalamnya mengalirkan bit-bit.

- Bus width: Banyak bit pada bus tersebut
- Data bus biasanya berkaitan dengan ukuran word, biasanya bus mengirim sebanyak 1 word, langsung mengirim ukuran data per block registernya.
- Address bus biasanya $\log_2(2^n)$ dari ukuran data, address bus digunakan untuk menyimpan alamat atau lokasi dari memori. Setiap address secara unik dapat mengidentifikasikan memori mana yang dituju.

## ISA (Instruction Set Architecture)

ISA ialah sekelompok instruksi yang bisa dilaksanakan oleh komputer. 📖

Apabila program kita tidak bisa diterjemahkan menjadi sekelompok instruksi pada ISA, maka tidak bisa dilaksanakan. Misalnya apabila pada ISA tidak ada penjumlahan dan pengurangan, akan sangat sulit untuk melakukan perkalian.

ISA menjembatani antara software dan hardware. Semua aplikasi harus bisa berdiri di atas ISA, harus bisa dirubah dalam perintah-perintah yang ada di ISA.

Misalnya, perkalian bisa dibuat menjadi penjumlahan yang berulang-ulang. 👍

Di bawah ISA, arsitektur von Neumann, di bawahnya ada datapath dan control, di bawahnya ada digital design, kemudian circuit design, dan layout. Dua layer terbawah, circuit design dan layout, dipelajari di Elektro. PSD dipelajari di Digital Design. Sementara POK akan memelajarai ISA, arsitektur, dan datapath & control.

Aplikasi, Sistem Operasi, Compiler akan dipelajari di DDP dan mata kuliah lain.

Tentunya suatu ISA yang sama dapat digunakan di beberapa mesin lain. Implementasinya terkadang berbeda-beda tapi. 😄

Hal yang harus diperhatikan pada design ISA:

- Susunan sistem memori yang digunakan, organisasi penyimpanan
- Tipe data dan struktur data, yang digunakan, cara memetakan dan representasinya
- Set instruksi
- Format instruksi
- Alamat dan cara mengakses data instruksi
- Kondisi lain

## Clock Cycles

Mensinkronisasikan perubahan data pada komputer, semua terjadi pada saat bersamaan. Harus ada standar kapan data pada sistem berubah.

Ada dua istilah yang biasa digunakan, frekuensi dan periode. Periode satuannya detik, frekuensi satuannya Hz.

$p = \frac{1}{f}$

$f = \frac{1}{p}$

Periode adalah waktu yang diperlukan dalam satu clock period (satu lembah, satu gunung)

Frekuensi ialah berapa banyak clock period dalam satu detik.

Misalkan periodenya $250\text{ns}$ maka frekuensinya ialah $4\text{MHz}$.

Semakin jarang atau panjang garis horizontalnya, periodenya cenderung lebih besar, frekuensinya lebih kecil. Begitu pula sebaliknya.

## CPU

CPU = Control Unit + ALU + Registers ✔️

- Control unit yang memonitor dan mengarahkan instruksi.
- ALU yang mengoperasikan operasi aritmetik dan logika. Pada ALU, ada select yang menentukan operasi apa yang dilakukan.
- Register ialah memori yang cepat di dalam CPU, menyimpan operand, dan hasil sementara. CPU tentu lebih cepat daripada RAM dan d. Saat ingin melakukan perhitungan, memori yang diperlukan akan disimpan di dalam register dulu, tidak bisa langsung dari memori. Baru nantinya akan diproses.
- Ada register khusus yang memang digunakan/dedicated untuk tugasnya:
    - PC (Program Counter) atau IP (Instruction Pointer) digunakan untuk menyimpan alamat nomor/urutan instruksi yang akan dilaksanakan selanjutnya, sangatlah penting untuk menentukan instruksi mana yang akan dilakukan terlebih dahulu dan belakangan.
    - ACC (Accumulator) digunakan untuk penyimpan hasil perhitungan sementara dan akhir, bisa sebagai operand.
    - IR (Instruction Register) digunakan untuk menyimpan instruksi yang akan digunakan/dieksekusi.
    - MAR (Memory Address Register) digunakan untuk menyimpan alamat memori.
    - MBR(Memory Buffer Register) atau MDR (Memory Data Register) menyimpan data yang ingin ditulis ke memory, atau yang baru dibaca dari memory.

    Referensi tambahan:

    - [https://www.ques10.com/p/8532/explain-role-of-different-registers-like-ir-pc-sp-/](https://www.ques10.com/p/8532/explain-role-of-different-registers-like-ir-pc-sp-/)
    - [http://indradwiantoro.blogspot.com/2010/10/tugas-organisasi-arsistektur-komputer.html](http://indradwiantoro.blogspot.com/2010/10/tugas-organisasi-arsistektur-komputer.html)

## Code Execution 🖱️

Langkah-langkahnya ialah:

- Kita buat program di High-Level languange
- Di-compile menjadi bahasa assembly
- Di-assemble menjadi bahasa mesin
- Menghubungkan beberapa bahasa mesin menjadi satu program
- Di-load ke memori komputer
- Program dieksekusi

Setelah disimpan dalam memori, setiap instruksi akan dilaksanakan dalam sebuah cycle: fetch, decode, execute.

- Instruction Fetch, instruksi dibawa ke register dari memori.
- Instruction Decode, menganalisa instruksi apa yang harus dilakukan.
- Operand Fetch, mengambil operand dan data yang dibutuhkan.
- Execute, dilakukannya operasi yang sesuai.
- Result Store, hasil dikembalikan ke memori.
- Next Instruction, menentukan instruksi apa yang akan dilaksanakan selanjutnya.

Proses ini terus berlangsung sampai program kita selesai.

### Fetch Example

Program kita diletakkan di memory secara berurutan. Nomor instruksi yang akan di-fetch akan diketahui pada register PC. Instruksi di-fetch dan pindah ke IR.

### Decode Example

Melihat operation dan operand yang diperlukan, diputuskan yang mana operand pertama, diletakkan di register mana, operand kedua, result akan diletakkan dimana, kemudian operasinya apa.

### Operand Fetch Example

Akan dicari operand dari memori, kemudian dipindahkan ke register yang bersesuaian. 

### Execute Example

Akan dilakukan di datapath. Akan dipilih melalui A select register operand pertama pada MUX a, serta dipilih register operand kedua pada MUX b menggunakan B select. Berikan instruksi G select sesuai operasi yang akan dilakukan kepada ALU. Kemudian MUX F dan MUX D akan mengarahkannya ke dalam load, menggunakan F dan D selectnya. Setelah itu, Load Enable akan dihidupkan supaya bisa menulis data ke dalam register, dan decoder akan memilih result kita akan disimpan ke register mana. 😵

Tentunya, bagian control unitlah yang menentukan select-select yang sesuai dengan instruksi.

### Result Store Example

Nilai register akan dibawa kembali ke memory

### Next Instruction Example

Program counter kemudian akan bertambah dan menjalankan instruksi atau operasi selanjutnya.

## Moore's Law

Gordon E. Moore meprediksi pada tahun 1965, kepadatan transistor akan mengganda setiap 18 bulan, artinya frekuensi clock akan semakin cepat. ☀️

Referensi:

- [https://www.investopedia.com/terms/m/mooreslaw.asp#:~:text=Moore's Law refers to Moore's,will pay less for them](https://www.investopedia.com/terms/m/mooreslaw.asp#:~:text=Moore's%20Law%20refers%20to%20Moore's,will%20pay%20less%20for%20them).

Perkembangan processor lebih cepat, sementara memori lebih lambat. Karena itu, gap antara processor dan memori semakin jauh, sekitar 50 cycle processornya mengganggur, untuk menunggu memorinya memberikan informasi.

Salam,

Hocky